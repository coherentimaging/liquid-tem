from lambda_tools.io import filter_shots

filter_shots('/nas/processing/serialed/20180906_Lysozyme/procdata/diffdata_4D.h5',
             '/nas/processing/serialed/20180906_Lysozyme/procdata/selected_4D.h5', 'peak_count >= 50', 70)