print('wtf')

import numpy as np

from lambda_tools.io import get_raw_stack, filter_shots, build_shot_list

filter_shots('/nas/processing/serialed/20180906_Lysozyme/procdata/diffdata_4D.h5',
             '/nas/processing/serialed/20180906_Lysozyme/procdata/selected_4D.h5', 'peak_count >= 50', 70)

if False:
        fns = ['/nas/temdata/Tecnai/ediff/20180906_Lysozyme/S2/Lysozyme_Cryo_027_00000.nxs',
                '/nas/temdata/Tecnai/ediff/20180906_Lysozyme/S2/Lysozyme_Cryo_027_00001.nxs',
                '/nas/temdata/Tecnai/ediff/20180906_Lysozyme/S2/Lysozyme_Cryo_030_00000.nxs',
                '/nas/temdata/Tecnai/ediff/20180906_Lysozyme/S2/Lysozyme_Cryo_030_00001.nxs']

        fns = ['/nas/temdata/Tecnai/ediff/20180906_Lysozyme/S2/Lysozyme_Cryo_027_00000.nxs',
                '/nas/temdata/Tecnai/ediff/20180906_Lysozyme/S2/Lysozyme_Cryo_030_00000.nxs']

        sl = build_shot_list(fns)
        darr, sl2 = get_raw_stack(sl, max_chunk=7, min_chunk=13)

        chunk_id = np.floor_divide(np.insert(np.array(darr.chunks[0]),0,0).cumsum()[:-1],10)
