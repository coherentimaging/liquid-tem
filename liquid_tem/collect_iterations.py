# Browses through an entire Relion Refinement directory, and converts ALL metadata into a single HDF file, which can be
# read by other codes more quickly

import pandas as pd
from pyem import star
import os
from collections import defaultdict


def stars_to_hdf(folder, it_nums = None, output_fn = 'rundata.h5'):

    # get iteration data from optimiser.star files
    file_list = os.listdir(folder)
    iter_files = [fn for fn in file_list if 'optimiser.star' in fn]
    iter_files.sort()
    iterations = pd.DataFrame()

    for fn in iter_files:
        iterations = iterations.append(star.parse_multi_star(folder + '/' + fn, keep_index=False)['data_optimiser_general'])

    # BUG: parse_multi_star only gets strings right for series. This is fixed in the next line...
    iterations = iterations.apply(pd.to_numeric, errors='ignore')

    iterations.set_index('rlnCurrentIteration', inplace=True)
    iterations = iterations.sort_index()
    iterations = iterations[~iterations.index.duplicated(keep='last')]

    # is there a second half (autorefinement)?
    halved = 'rlnModelStarFile2' in iterations.columns

    # check if there is a final-iteration file (for completed autorefinements) and add an entry in the optimizer table
    fn = iterations.iloc[-1]['rlnOutputRootName']
    if os.path.isfile(fn + '_data.star'):
        iterations.loc[999,:] = iterations.iloc[-1,:].copy()
        iterations.loc[999,'rlnModelStarFile'] = fn + '_model.star'
        iterations.loc[999, 'rlnExperimentalDataStarFile'] = fn + '_data.star'
        iterations.loc[999, 'rlnOrientSamplingStarFile'] = fn + '_sampling.star'

    # alliter will finally be a dict, containing DataFrames that correspond to each table in the star files
    alliter = defaultdict(pd.DataFrame)

    alliter['data_optimiser_general'] = iterations

    for idx, it in iterations.iterrows():

        # FIRST PART: All star files except for the model file(s). This is the easy part.

        print('Reading Iteration {}'.format(idx))
        m = {} # m holds all the tables from the new iteration

        print('Parsing particle data file: ' + it['rlnExperimentalDataStarFile'])
        m['data_images'] = star.parse_star(it['rlnExperimentalDataStarFile'], keep_index=False)

        print('Parsing orientation data file: ' + it['rlnOrientSamplingStarFile'])
        m.update(star.parse_multi_star(it['rlnOrientSamplingStarFile'], keep_index=False))

        # now iterate through all tables and append them to the full ones
        for k, v in m.items():
            v = v.apply(pd.to_numeric, errors='ignore')
            if isinstance(v, pd.Series):
                v = v.to_frame().T
            v['rlnCurrentIteration'] = idx
            alliter[k] = alliter[k].append(v)

        # SECOND PART: now the model files are read. These are more complex and tricky. Also they take forever to parse,
        # as they contain so many tables (one for each group/class)

        print('Parsing model data file (may take long!): ' + it['rlnModelStarFile'])
        mm = [] # this is required to potentially hold two sets of data frames - will be a list of dicts
        mm.append(star.parse_multi_star(it['rlnModelStarFile'], keep_index=False))
        if halved and not (idx == 999):
            print('Parsing second-half model data file (may take long, too!): ' + it['rlnModelStarFile2'])
            mm.append(star.parse_multi_star(it['rlnModelStarFile2'], keep_index=False))

        grplist = []
        cllist = []

        # loop over (potentially two) model files
        for hid, m in enumerate(mm):
            # within this loop, m is as in that above

            for k in m.keys():
                m[k]['rlnCurrentIteration'] = idx
                if halved:
                    m[k]['rlnRandomSubset'] = hid + 1
                else:
                    # do not use subsets
                    m[k]['rlnRandomSubset'] = 0

            for k in ['data_model_groups', 'data_model_classes']:
                alliter[k] = alliter[k].append(m[k])
            alliter['data_model_general'] = alliter['data_model_general'].append(m['data_model_general'].to_frame().T)

            # now here's the tricky part: iterate over all groups, and append their group data to single dataframe
            for idx2, it in m['data_model_groups'].iterrows():
                # print (it['rlnGroupName'], it['rlnGroupNrParticles'])
                if it['rlnGroupNrParticles'] == 0:
                    # groups can be empty!
                    continue
                newgrp = m['data_model_group_{}'.format(it['rlnGroupNumber'])]
                newgrp['rlnGroupNumber'] = it['rlnGroupNumber']
                grplist.append(newgrp)

            # same for classes
            for idx2, it in m['data_model_classes'].iterrows():
                # print (it['rlnReferenceImage'], it['rlnClassDistribution'])
                newcl = m['data_model_class_{}'.format(idx2 + 1)]
                newcl['rlnClassNumber'] = idx2 + 1
                cllist.append(newcl)

        alliter['data_model_class_all'] = alliter['data_model_class_all'].append(pd.concat(cllist))
        alliter['data_model_group_all'] = alliter['data_model_group_all'].append(pd.concat(grplist))

    # make sure that all numeric columns are registered as such
    for k, v in alliter.iteritems():
        alliter[k] = v.apply(pd.to_numeric, errors='ignore')

    # now, set proper indexing on all tables

    # backup in case something goes wrong... (these are inplace operations!)
    #alliter_bak = alliter.copy()

    print('Defining index columns...')
    alliter['data_sampling_general'].set_index('rlnCurrentIteration', inplace=True)

    alliter['data_sampling_directions']['Idx'] = alliter['data_sampling_directions'].index
    alliter['data_sampling_directions'].set_index(['rlnCurrentIteration', 'Idx'], inplace=True)

    alliter['data_images']['Idx'] = alliter['data_images'].index
    alliter['data_images'].set_index(['rlnCurrentIteration', 'Idx'], inplace=True)

    alliter['data_model_group_all'].set_index(['rlnCurrentIteration', 'rlnRandomSubset',
                                               'rlnGroupNumber', 'rlnSpectralIndex'], inplace=True)

    alliter['data_model_class_all'].set_index(['rlnCurrentIteration', 'rlnRandomSubset',
                                               'rlnClassNumber', 'rlnSpectralIndex'], inplace=True)

    alliter['data_model_classes']['rlnClassNumber'] = alliter['data_model_classes'].index + 1
    alliter['data_model_classes'].set_index(['rlnCurrentIteration', 'rlnRandomSubset', 'rlnClassNumber'], inplace=True)

    alliter['data_model_groups'].set_index(['rlnCurrentIteration', 'rlnRandomSubset', 'rlnGroupNumber'], inplace=True)

    # finally write into HDF store
    with pd.HDFStore(folder + '/' + output_fn, mode='w') as store:
        for k, v in alliter.iteritems():
            print('Writing table ' + k + ' to HDF file.')
            store.put(k, v, format='table', data_columns=True)

