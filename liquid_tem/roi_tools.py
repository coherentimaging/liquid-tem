# -*- coding: utf-8 -*-
"""
Created on Tue Mar 21 19:38:01 2017

@author: bueckerr
"""

import numpy as np
from scipy.interpolate import interp2d

def get_roi_stats(img, rois):    
    
    stats = {}   
    select = []
    k = 0
    
    if not isinstance(img,np.ndarray):
        img = np.array(img)
    
    for roi in rois:
        
        k += 1        
        crop = img[roi['y']:(roi['y']+roi['h']), roi['x']:(roi['x']+roi['w'])]
        vec = crop.reshape(-1)
        #vec = vec.astype(np.float32)
        stats.update({'Mean' + str(k): vec.mean(), 'Var' + str(k): vec.var(), 'StdDev' + str(k): vec.std(), \
                      'Min' + str(k): vec.min(), 'Max' + str(k): vec.max(), 'Size' + str(k): vec.size, \
                      'Median' + str(k): np.median(vec)})
        select.append(crop)
    
    return stats, select

def get_background(img, rbcSize = 10, roi = []):
    """
    estimates background from four corners of given ROI. returns collated background
    statistics, an interpolation fuction for the background, full statistics of all corners
    """
    
    if not isinstance(img,np.ndarray):
        img = np.array(img)
    
    sz = img.shape
    
    if not roi:
        roi = {'x': 0, 'y': 0, 'w': sz[1], 'h': sz[0]}
    
    # regions for background correction: list of tuples
    rbcs = [{'x': roi['x'], 'y': roi['y'], 'w': rbcSize, 'h': rbcSize},
            {'x': roi['x'] + roi['w'] - rbcSize, 'y': roi['y'], 'w': rbcSize, 'h': rbcSize},
            {'x': roi['x'], 'y': roi['y'] + roi['h'] - rbcSize, 'w': rbcSize, 'h': rbcSize},
            {'x': roi['x'] + roi['w'] - rbcSize, 'y': roi['y'] + roi['h'] - rbcSize, 'w': rbcSize, 'h': rbcSize}]

    # print(rbcs)
    
    all_stats, sel = get_roi_stats(img, rbcs)
    
    ip = interp2d([roi['x'] + rbcSize/2, roi['x'] + roi['w'] - rbcSize/2],
                  [roi['y'] + rbcSize/2, roi['y'] + roi['h'] - rbcSize/2],
                  [[all_stats['Mean1'], all_stats['Mean2']],[all_stats['Mean3'], all_stats['Mean4']]])
    
    stats = {'AvgBGMean': (all_stats['Mean1']+all_stats['Mean2']+all_stats['Mean3']+all_stats['Mean4'])/4,
             'AvgBGStd': (all_stats['StdDev1']+all_stats['StdDev2']+all_stats['StdDev3']+all_stats['StdDev4'])/4,
             'StdBGMean': np.array([all_stats['Mean1'],all_stats['Mean2'],all_stats['Mean3'],all_stats['Mean4']]).std()}
    
    return stats, ip, all_stats
    