# clean_autopick reads the autopick result from each micrograph, and cleans it up by:
# (1) only picking the particle with the largest FOM
# (2) applying a lower threshold on the FOM (can be different from that set in RELION)
# (3) only keeping particles that are less than a given number of pixels away from the previous shot
#
# Currently only sorts files by name, does not (yet) import metadata to sort by date... to be implemented soon

import os
import re

import pandas as pd
from pyem import star


def clean_autopick(coord_dir_in, coord_dir_fixed, threshold = 0.05, max_shift = 50):

    if coord_dir_in == coord_dir_fixed:
        raise Exception('Input and output directory must not be the same!')

    if not os.path.exists(coord_dir_fixed):
        os.makedirs(coord_dir_fixed)
    fns = [each for each in os.listdir(coord_dir_in) if each.endswith('pick.star')]

    fns.sort()

    DF = pd.DataFrame()
    k = 0

    for fn in fns:

        # find rows where a new data collection is starting
        dcnum = int(re.search('DC(\d)',fn).group(1))

        print('%i: %s (subset %i)') % (k, fn, dcnum)

        try:
            coord = star.parse_star(coord_dir_in + '/' + fn, keep_index=False)

        except:
            print('No coordinates in this file. Skipping.')
            continue

        # select only the picked position with the highest autopick FOM
        coord.sort_values('rlnAutopickFigureOfMerit', inplace=True, ascending=False, axis=0)
        coord_fixed = coord.drop(coord.index[1:]).reset_index(drop=True)

        # allow to re-introduce a threshold, as well as other criteria
        if coord_fixed.loc[0, 'rlnAutopickFigureOfMerit'] < threshold:
            print('Particle falls below FOM threshold')
            continue

        if (k > 0) \
                and (abs(coord_fixed.loc[0, 'rlnCoordinateX'] - DF['rlnCoordinateX'].iloc[-1]) > max_shift)\
                and dcnum == DF['dc'].iloc[-1]:
            print('X coordinate distance too large')
            continue

        if (k > 0) \
                and (abs(coord_fixed.loc[0, 'rlnCoordinateY'] - DF['rlnCoordinateY'].iloc[-1]) > max_shift) \
                and dcnum == DF['dc'].iloc[-1]:
            print('Y coordinate distance too large')
            continue

        star.write_star(coord_dir_fixed + '/' + fn, coord_fixed)
        DF = DF.append(coord_fixed.iloc[0, :], ignore_index=True)

        DF.ix[k, 'fn'] = fn
        DF.ix[k, 'dc'] = dcnum
        k += 1

    DF.sort_values('fn', inplace=True)
    DF.reset_index(inplace=True)
    DF.to_csv('pick.csv')
    print('Done. Wrote file pick.csv for further analysis')

    return DF

if __name__ == '__main__':

    # PARAMETERS
    # original Autopick result
    coord_dir_in = '/media/eggert/DATA/liquid-data/20170314/AutoPick/AP_Gen1/MicrographsOld'
    # cleaned-up Autopick directory
    coord_dir_fixed = '/media/eggert/DATA/liquid-data/20170314/AutoPick/AP_Gen1/Micrographs'
    # Autopick FOM threshold
    threshold = 0.2
    # Maximum shift w.r.t. previous shot
    max_shift = 50

    clean_autopick(coord_dir_in, coord_dir_fixed, threshold, max_shift)
