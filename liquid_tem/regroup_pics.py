"""
Functions for data mangling in particle star files. Somewhat extends the star class from pyem (but not quite).
Makes use of pandas a lot.
"""

from glob import glob
from pyem import star
import numpy as np
import pandas as pd


def get_groups(meta, groupsize = 10):

    # check for micrograph name field
    for mf in ['micrograph', 'rlnMicrographName']:
        if mf in meta.columns: break

    # sort meta data based on time or micrograph name
    if 'time' in meta.columns:
        meta_sorted = meta.sort_values(['time', mf])
        print('Timestamps found: grouping based on time')
    else:
        meta_sorted = meta.sort_values(mf)
        print('No timestamps found: grouping based on micrograph name')

    # assign new groups, not account for collection series/bursts yet
    new_group = np.floor(np.arange(len(meta_sorted)) / float(groupsize)).astype('int')

    # find rows where a new data collection is starting
    dcpos = meta_sorted[mf].str.extract('DC(\d)', expand=False)

    if not dcpos.isnull().any():
        print('\'DC\' in micrograph names found: skipping group at new data collections.')
        # find jumps
        new_run = dcpos.astype('int').diff().astype('bool')
        new_pos = new_run[new_run == True].index

        # introduce jump at each DC boundary
        for k in new_pos:
            new_group[k:] = new_group[k:] + 1

    else:
        print('No \'DC\' in micrograph name found: not skipping group at new data collections.')
        new_group = new_group + 1

    meta_sorted['group'] = new_group.astype(int)

    meta_sorted['groupname'] = 'group_' + meta_sorted['group'].astype(str)
    # merge groups back into original order

    return meta.merge(right=meta_sorted, on=mf)[['group', 'groupname']]


def regroup(input, output, groupsize = 10, metafile = None, metafile_key = 'f', add_id=True):
    
    # read star file
    rdat = star.parse_star(input, keep_index=False)

    print('loaded ' + input)
    rdat.dropna(axis=0, how='all', inplace=True)

    if metafile is None:
        meta = rdat
        groups = get_groups(meta, groupsize)
    else:
        meta = rdat.merge(pd.read_hdf(metafile, metafile_key), how='left', left_on='rlnMicrographName', right_on='micrograph')
        groups = get_groups(meta, groupsize)

    rdat['rlnGroupName'] = groups['groupname']

    if add_id:
        rdat['rlnParticleId'] = rdat.index

    star.write_star(output, rdat, reindex=True)
    print('wrote ' + output)

    return meta


def shuffle(input, output, undo=False, undo_by_fn=False, use_id=True):
    """
    randomizes particle positions in the star file, or un-randomizes them again (which needs the rlnParticleId
    column to be present, or well-behaved micrograph names)
    """

    rdat = star.parse_star(input, keep_index=False)

    if undo:
        if ('rlnParticleId' in rdat.columns) and not undo_by_fn:
            rdat.sort_values(by='rlnParticleId', inplace=True)
            print('Sorting particles by ID.')
        else:
            rdat.sort_values(by=['rlnMicrographName', 'rlnParticleName'], inplace=True)
            print('Sorting particles by Micrograph Name.')

    else:
        if ('rlnParticleId' not in rdat.columns) and use_id:
            rdat['rlnParticleId'] = rdat.index
        elif ('rlnParticleId' in rdat.columns) and not use_id:
            rdat.drop('rlnParticleId', axis=1, inplace=True)

        rdat = rdat.sample(frac=1)
        print('Randomizing particle positions.')

    star.write_star(output, rdat.reset_index(drop=True), reindex=True)
    print('Wrote particles file {}.'.format(output))


def broadcast_coordinates(avg_star, sgl_star, avg_dir='Averages', sgl_dir='Micrographs',
                          groupsize = 20, angles = False, allcolumns = False, postfix_level = 1, required_cols = None):
    """
    Broadcasts coordinates from particles derived from one set of Micrographs (in avg_dir) to a different set of
    Micrographs (in sgl_dir). The postfix_level parameter sets how many underscore-separated postfixes are appended
    to corresponding micrographs in sgl_dir (e.g. "image_0001_42_313.mrc" in sgl_dir will be matched to
    "image_0001_42.mrc" if postfix_level = 1, but to "image_0001" if postfix_level = 2). Useful to e.g. run
    autopicking on averaged images, then extract from single images while keeping the coordinates. Can regroup right
    away (using groupsize parameter).
    """

    pavg = star.parse_star(avg_star, keep_index=False)


    psgl = pd.DataFrame()
    psgl['rlnMicrographName'] = glob(sgl_dir + '/*.mrc')

    psgl.sort_values(by='rlnMicrographName', inplace=True)
    psgl.reset_index(drop=True, inplace=True)

    snames = pd.DataFrame([x.split('/', 1)[1:][0].rsplit('.mrc')[0] for x in psgl['rlnMicrographName']])
    anames = pd.DataFrame([x.split('/', 1)[1:][0].rsplit('.mrc')[0] for x in pavg['rlnMicrographName']])

    if anames.loc[0,0].find(snames.loc[0,0]) >= 0:
        an_short = pd.DataFrame([an[0:len(sn)] for (an, sn) in zip(anames[0].tolist(), snames[0].tolist())])
        an_short['idx'] = an_short.index
        sn_idx = snames.merge(an_short, how='left', on=0).fillna(method='pad')
    else:
        pass

    psgl['AvgMicrographName'] = pavg.loc[sn_idx['idx'],'rlnMicrographName'].reset_index(drop=True)

    # generate column that contains averaged micrograph name corresponding to each single micrograph
    #psgl['AvgMicrographName'] = [avg_dir + '/' + x.split('/', 1)[1:][0].rsplit('_', postfix_level)[:-1][0] + '.mrc'
    #                             for x in psgl['rlnMicrographName']]

    print(psgl.tail(5))

    # columns to be taken over from average particle data
    if allcolumns:
        required_cols = list(pavg.columns)

    elif required_cols == None:
        # Standard columns (available after 2D Class Averaging)
        required_cols = ['rlnCoordinateX', 'rlnCoordinateY', 'rlnMicrographName',
                           'rlnMagnification', 'rlnDetectorPixelSize', 'rlnAutopickFigureOfMerit', 'rlnOriginX', 'rlnOriginY']

        if angles:
            # Nice for multi-step refinemens
            required_cols.extend(['rlnAngleRot','rlnAngleTilt','rlnAngleDiff'])

    if 'rlnImageName' in required_cols:
        # we NEVER want the image name, as this is necessarily inconsistent with the output file and may cause trouble
        required_cols.remove('rlnImageName')

    # clean up non-existing columns
    required_cols = [c for c in required_cols if c in list(pavg.columns)]
    print(required_cols)

    psgl = psgl.merge(pavg[required_cols],
                      left_on='AvgMicrographName', right_on='rlnMicrographName',
                      suffixes=('', '_avg')).drop(['AvgMicrographName', 'rlnMicrographName_avg'], axis=1)

    star.write_star(sgl_star, psgl)

    if groupsize is not None:
        regroup(sgl_star, sgl_star, groupsize=groupsize)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('input', type=str, help='Input file')
    parser.add_argument('output', type=str, help='Output file')
    parser.add_argument('--groupsize', type=int, default=10, help='Group size')
    parser.add_argument('--metafile', type=str, default=None, help='Location of HDF meta data file')
    parser.add_argument('--metafile_key', type=str, default='f', help='HDF key of micrograph table')

    args = parser.parse_args()
    regroup(args.input, args.output, args.groupsize, args.metafile, args.metafile_key)