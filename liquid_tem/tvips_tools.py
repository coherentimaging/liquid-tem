# Tools for processing files from the TVIPS camera
# Mostly does nothing anymore, as the tifffile library now directly supports TVIPS metadata

import numpy as np
from tifffile import TiffFile
import struct


def read_tvips(filename):
    """
    reads and returns TVIPS camera image and metadata.
    """

    image = TiffFile(filename)
    tags = image[0].tags
    # off = tags['37706'].value
    # tvips_dat = extract_meta(fp, off)
    tvips_dat = image[0].tags['tvips_metadata'].value
    img = image.asarray()
    image.close()

    return img, tvips_dat, tags
           

def extract_meta(fp, off):
    # extracts metadata from file handle, needs the offset where metadata starts
    # offset can be obtained by reading TIF image tag 37706, e.g. using PIL
    
    intFmt = 'i'
           
    fp.seek(off+244)
    tvips_dat = {'name': fp.read(160).decode('UTF-16'),
                 'folder': fp.read(160).decode('UTF-16'),
                 'size_x': struct.unpack(intFmt,fp.read(4))[0],
                 'size_y': struct.unpack(intFmt,fp.read(4))[0]}
                 
    fp.seek(off+580)
    tvips_dat.update({'data_type': struct.unpack(intFmt,fp.read(4))[0],
                      'date': struct.unpack(intFmt,fp.read(4))[0],
                      'time': struct.unpack(intFmt,fp.read(4))[0],
                      'comment': fp.read(1024).decode('UTF-16'),
                      'history': fp.read(1024).decode('UTF-16'),
                      'scaling': struct.unpack('ffffffffffffffff',fp.read(64))})
    fp.seek(off+2968)  
    tvips_dat.update({'image_dist_x': struct.unpack('f',fp.read(4))[0],
                      'image_dist_y': struct.unpack('f',fp.read(4))[0]})
    fp.seek(off+3272)  
    tvips_dat.update({'voltage': struct.unpack('f',fp.read(4))[0],
                      'aberrations': struct.unpack('ffffffffffffffffffffffffffffffff',fp.read(32*4))})
    fp.seek(off+3532)  
    tvips_dat.update({'tem_mode': struct.unpack(intFmt,fp.read(4))[0],
                      'magnification': struct.unpack('f',fp.read(4))[0],
                      'magnification_correction': struct.unpack('f',fp.read(4))[0],
                      'post_magnification': struct.unpack('f',fp.read(4))[0]})      
    fp.seek(off+3552)  
    tvips_dat.update({'stage_position': struct.unpack('fffff',fp.read(20)),
                      'image_shift': struct.unpack('ff',fp.read(8)),
                      'beam_shift': struct.unpack('ff',fp.read(8)),
                      'beam_tilt': struct.unpack('ff',fp.read(8))})                
    fp.seek(off+3624)  
    tvips_dat.update({'illumination': struct.unpack('fff',fp.read(12))})         
    fp.seek(off+3952)  
    tvips_dat.update({'exposure_time': struct.unpack('f',fp.read(4))[0]})  
    fp.seek(off+4124)  
    tvips_dat.update({'sensitivity': struct.unpack('f',fp.read(4))[0],
                      'dose_on_cam': struct.unpack('f',fp.read(4))[0]})          
    
    return tvips_dat