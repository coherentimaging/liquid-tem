from pyem import star, mrc
from glob import glob
import numpy as np

def stack_from_single(input_dir='.', output_dir='.', file_pattern='*.mrc', output_format='mrcs',
                      separator='_', sep_level=1):

    fns_all = glob(input_dir + '/' + file_pattern)
    fns_all.sort()

    fn_root = list(set([fn.rsplit(separator, sep_level)[0] for fn in fns_all]))
    fn_root.sort()

    for fn_new in fn_root:

        fns_sgl = [fn for fn in fns_all if fn_new in fn]
        imgs_sgl = []

        for fn in fns_sgl:
            imgs_sgl.append(mrc.read(fn))

        fn_out = output_dir + '/' + fn_new.rsplit('/',1)[-1] + '.' + output_format
        mrc.write(fn_out, np.concatenate(imgs_sgl, axis=2))

        print('Merged {} images into {}'.format(len(fns_sgl),fn_out))
