from distutils.core import setup

setup(
    name='liquid-tem',
    version='0.12',
    packages=['liquid_tem'],
    url='',
    license='',
    author='Robert Bücker',
    author_email='robert.buecker@mpsd.mpg.de',
    description='', requires=['h5py']
)
