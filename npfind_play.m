clear
folder = 'D:\temp\AuNP';
files = dir([folder '\*.tif']);

%fn = [folder '\' 'AuNp20nm_2_30C_0001.tif'];
fn = [folder '\' files(50).name];
img = tiffread(fn);

cl = [0.02,0.6];

figure(1), clf
for k = 6:-1:1
    axs(k) = subplot(3,2,k);
end

border = 200;

%%
set(gcf,'CurrentAxes',axs(1))
dat = double(img.data); 
dat = dat(:,border+1:end);
dat = dat./max(dat(:));
%dat = imadjust(dat,stretchlim(dat,0.1));
%dat = adapthisteq(dat,'ClipLimit',0.01);
imagesc(dat)
caxis(stretchlim(dat,cl))
colorbar

%%
set(gcf,'CurrentAxes',axs(2))
%datf = imgaussfilt(dat,4);
datf = wiener2(dat,[5 5]);
%datf = dat;
imagesc(datf)
caxis(stretchlim(datf,cl))
colorbar
%impixelinfo

%%
set(gcf,'CurrentAxes',axs(3))
seNP = strel('ball',3,0);
dato = imopen(datf,seNP);
imagesc(dato)
caxis(stretchlim(dato,cl))
colorbar

%% maybe do the adjust BEFORE the close etc.
cle = [0.01, 0.5];
set(gcf,'CurrentAxes',axs(4))
seNP = strel('ball',3,0);
datf = imadjust(datf,stretchlim(datf,cle));
%date = datf;
date = imclose(datf,seNP);
date = imadjust(date,stretchlim(date,cle));
imagesc(date)
%caxis(stretchlim(date,cl))
colorbar

%%
set(gcf,'CurrentAxes',axs(5))
datm = imextendedmin(date,0.04);
imagesc(datm)
%caxis(stretchlim(datm,cl))
%%
linkaxes(axs)


%%
cor = datm - mean(datm(:));
acf = convnfft(cor,rot90(cor,2),'same');

figure(2)
imagesc(acf)

%% radial average

ctr = [1024-border, 1024];

[X,Y] = meshgrid((1:size(acf,2))-ctr(1),(1:size(acf,1))-ctr(2));
[theta, r] = cart2pol(X,Y);

maxr = 700;

[N, ~, idcs] = histcounts(r(:),0:maxr);

pts = acf(:);
pts(idcs == 0) = [];
idcs(idcs == 0) =[];

sumSig = sparse(idcs, 1, pts, maxr, 1);
NSig = sparse(idcs, 1, 1, maxr, 1);
meanSig = sumSig./NSig;

figure(3)
plot(0:maxr-1,meanSig)
