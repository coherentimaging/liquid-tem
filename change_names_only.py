# -*- coding: utf-8 -*-
"""
Created on Fri Mar 17 16:13:59 2017

Metadata extractor for TVIPS images. Works on an entire directory.
Optionally merges ImageJ measurement results into metadata.
Exports as CSV, HDF5, and Pandas Pickle.

@author: bueckerr
"""

import numpy as np
import pandas as pd
import os
from tifffile import TiffWriter
#from EMAN2 import EMData, EMNumPy
from pyem import mrc, star
#import sys
#sys.path.append('/home/eggert/EMAN2/bin')
#import em2proc2d

#%% get tif files from directories, read file names and dates, make list

fields = ['tem_beam_shift', 'tem_beam_tilt', 'dose', 'exposure_time',
          'pixel_size_x','pixel_size_y','tem_illumination','tem_image_shift',
          'tem_magnification','tem_stage_position','sensitivity']

baseFolder = 'T:/TEM_data/Eike/20170620'
inputFolders = ['T7_DC1', 'T7_DC2', 'T7_DC3']
saveAsTiff = false
saveAsMRC = true
workingFolder = 'T:/TEM_data/Eike/20170620'
outputFolder = 'Micrographs'

# BROWSE FILES

fileList = pd.DataFrame()

k = 0
for sub_dir in inputFolders:
    fns = [each for each in os.listdir(baseFolder + '/' + sub_dir) if each.endswith('.tif')]
    times = [os.path.getmtime(baseFolder + '/' + sub_dir + '/' + fn) for fn in fns]
    fdat = {'sub_dir': sub_dir, 'input_file': fns, 'time': pd.to_datetime(times,unit = 's')}
    fdat.update(ROI[k])
    fileList = fileList.append(pd.DataFrame(fdat), ignore_index = True)
    k += 1

if not os.path.exists(workingFolder + '/' + outputFolder + '/'):
    os.makedirs(workingFolder + '/' + outputFolder + '/')

infos = []

#%% outer loop: over micrographs. Cuts bursts into single pictures.
for k in range(len(fileList)):

    p = fileList.iloc[k]
    
    for m in range(imgShots):
        
        infom = info.copy()
        
        path = workingFolder + '/' + outputFolder + '/'
        fn = p.sub_dir + '_' + p.input_file[:-4] + '_{:03d}'.format(m+1)

        if saveAsTiff:
            infom['micrograph'] = outputFolder + '/' + fn + '.tif'

            imgOut = TiffWriter(path + fn + '.tif')
            imgOut.save(imgROI.astype(np.float32))
            imgOut.close()

        if saveAsMRC:
            infom['micrograph'] = outputFolder + '/' + fn + '.mrc'
            mrc.write(path + fn + '.mrc',imgROI[:,:,np.newaxis])
        
        # append meta data to dict list
        infos.append(infom)

meta = pd.DataFrame(infos)
meta['ID'] = meta.index
#%%
meta.to_csv(workingFolder + '/meta.csv')
meta.to_pickle(workingFolder + '/meta.pkl')
meta.to_hdf(workingFolder + '/meta.h5','f',mode='w')

starData = pd.DataFrame()
starData['rlnMicrographName'] = meta['micrograph']
starData['rlnMicrographId'] = meta['ID']
starData['rlnMagnification'] = meta['tem_magnification'] * 1.28
starData['rlnDetectorPixelSize'] = 15.6

#starData['rlnDose'] = meta['dose_calibrated']
#starData['sub_dir'] = meta['sub_dir']
#starData['dose_calibrated'] = meta['dose_calibrated']

star.write_star(workingFolder + '/init_meta.star',starData)

#meta.to_hdf(basedir + '/meta.h5','table')