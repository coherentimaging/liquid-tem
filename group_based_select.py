from pyem import star
import pandas as pd
import numpy as np

data_in = '2DCA_035/run_it025_data.star'
data_out = '2DCA_035/run_it025_data_GSC.star'
model_in = '2DCA_035/run_it025_model.star'
meta = 'meta.h5'
keep = 50 # in percent

# load data
model = star.parse_multi_star(model_in, keep_index = False)
particles_in = star.parse_star(data_in, keep_index = False)
meta = pd.read_hdf('meta.h5','f')

# Transmission through cell
meta['transmission'] = meta['AvgBGMean']/meta['sensitivity'] \
            /(meta['pixel_size_x']*meta['pixel_size_y']*1e18)/meta['dose_calibrated']

# merge metadata into particles table
particles_sel = particles_in.merge(meta[['micrograph','dose_calibrated']], how='left', left_on='rlnMicrographName', right_on='micrograph')

# Groups table
groups = model['data_model_groups']

# merge GSC into particles table
particles_sel = particles_sel.merge(groups[['rlnGroupNumber', 'rlnGroupScaleCorrection']], how='left', on='rlnGroupNumber')

# custom FOM definition
particle_FOM = particles_sel['rlnGroupScaleCorrection']/np.sqrt(particles_sel['dose_calibrated'])

particles_out = particles_in.loc[particle_FOM >= particle_FOM.quantile((100-keep)/100.),:]

star.write_star(data_out, particles_out)