from glob import glob

import dask.array as da
import h5py
import numpy as np
from dask.diagnostics import Profiler, ResourceProfiler
from tifffile import TiffFile

from lambda_tools import proc2d, compute

with TiffFile('Ref_reference.tif') as tif: reference=tif.asarray()
with TiffFile('Ref_pxmask.tif') as tif: pxmask=tif.asarray()

fns = glob('CatalaseStrong_0000?.nxs')
fns.sort()
darr = []
for fn in fns:
    ds = h5py.File(fn,'r')['/entry/instrument/detector/data']
    darr.append(da.from_array(ds, chunks=(16,516,1556)))

darr = da.concatenate(darr)
darr2 = proc2d.apply_flatfield(darr, reference)
darr3 = proc2d.correct_dead_pixels(darr2, pxmask, strategy='replace', replace_val=-1, mask_gaps=True)

com = proc2d.center_of_mass2(darr3, threshold=10)

lorentz = compute.map_reduction_func(darr3, proc2d.lorentz_fit_moving, com, verbose=False, output_len=30)


def quant(x, q):
    if isinstance(x, np.ndarray):
        return np.sort(x)[np.round(len(x) * q).astype(int)]
    else:
        return x

									 
funcs = (np.mean, np.median, lambda a: quant(a, 0.1))
prof = compute.map_reduction_func(darr3, proc2d.radial_proj, lorentz[:, 1],
                                  lorentz[:,2],
                                  my_func=funcs,
                                  max_size=400, output_len=400)

with ResourceProfiler() as rprofile, Profiler() as profile:
    p = prof[:64].compute()



#funcs = (np.mean, np.median, lambda a: proc2d.mean_clip(a, 1.9))



# imgs, fits = da.compute(darr3[16384:16400,...], lorentz[16384:16400,...])
#p = []
#for idx, (ii, ff) in enumerate(zip(imgs, fits)):
#    print(idx)
#    p.append(proc2d.radial_proj(ii, ff[1], ff[2], my_func=funcs, max_size=400))

#da.to_hdf5('profiles_catalase_strong_2.h5', {'/prof': prof, '/com': com, '/lorentz': lorentz})
