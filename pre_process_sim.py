# -*- coding: utf-8 -*-
"""
Created on Fri Mar 17 16:13:59 2017

Metadata extractor for TVIPS images. Works on an entire directory.
Optionally merges ImageJ measurement results into metadata.
Exports as CSV, HDF5, and Pandas Pickle.

@author: bueckerr
"""

import os
import re
from shutil import copyfile
import numpy as np
import pandas as pd
# from EMAN2 import EMData, EMNumPy
from pyem import mrc, star
from liquid_tem.roi_tools import get_background
from tifffile import TiffFile, TiffWriter
from liquid_tem.tvips_tools import read_tvips

#import sys
#sys.path.append('/home/eggert/EMAN2/bin')
#import em2proc2d

#%% get tif files from directories, read file names and dates, make list

fields = []

baseFolder = '/scratch/Micrographs_sim_rndT7_v2'
inputFolders = ['']

workingFolder = '/scratch/simulated_H'
outputFolder = 'Micrographs'
avgFolder = 'Averages'

# ROI: list of dictionaries, each entry corresponds to one input folder
ROI = [{'x': 0, 'y': 0, 'w': 800, 'h': 800}]
#ROI = [{'x': 0, 'y': 0, 'w': 2048, 'h': 300}]

# which type of corrections shall be applied?
BGCorr = True
Normalize = True
bilinearBGCorr = True
singleCorr = False

# which output files shall be written?
makeAvgs = False
saveAsTiff = False
saveAsMRC = True

# geometry: barely needs adjustment
imgHeight = 800
rbcSize = 50

# clean up burst background noise: required if ROI covers uppermost part
cleanBurstNoise = False
burstNoiseRef = 'T:\TEM_data\Calibration\BurstNoiseRef.tif'
skipFirst = True


# BROWSE FILES

fileList = pd.DataFrame()

k = 0
for sub_dir in inputFolders:
    fns = [each for each in os.listdir(baseFolder + '/' + sub_dir) if each.endswith('.tif')]
    times = [os.path.getmtime(baseFolder + '/' + sub_dir + '/' + fn) for fn in fns]
    fdat = {'sub_dir': sub_dir, 'input_file': fns, 'time': pd.to_datetime(times,unit = 's')}
    fdat.update(ROI[k])
    fileList = fileList.append(pd.DataFrame(fdat), ignore_index = True)
    k += 1

if not os.path.exists(workingFolder + '/' + outputFolder + '/'):
    os.makedirs(workingFolder + '/' + outputFolder + '/')
    
if makeAvgs and not os.path.exists(workingFolder + '/' + avgFolder + '/'):
    os.makedirs(workingFolder + '/' + avgFolder + '/')    

if cleanBurstNoise:
    with TiffFile(burstNoiseRef) as img:
        bnRef = img.asarray().astype(float)

infos = []

#%% outer loop: over micrographs. Cuts bursts into single pictures.

path = workingFolder + '/' + outputFolder + '/'
avgpath = workingFolder + '/' + avgFolder + '/'

for k in range(len(fileList)):

    p = fileList.iloc[k]
    
    try:
        # start info dict by grabbing metadata from TVIPS TIF
        image, info, tags = read_tvips(baseFolder + '/' + p.sub_dir + '/' + p.input_file)
    except:
        img = TiffFile(baseFolder + '/' + p.sub_dir + '/' + p.input_file)
        image = img.asarray()
        info = {'comment': ''}
        img.close()
        #print('Warning: could not read TIFF file: ' + p.input_file)
        #continue
    
    # parse string(s) from comment fields    
    doseStr = re.findall('D=(\d+(\.\d*)?)', info['comment'])
    intStr = re.findall('CL3=(\d+(\.\d*)?)', info['comment'])
    spotStr = re.findall('S=(\d+(\.\d*)?)', info['comment'])
    info = dict((m, info[m]) for m in fields)
    if not len(doseStr) == 0:
        info.update({'dose_calibrated': float(doseStr[0][0])})
    if not len(intStr) == 0:
        info.update({'intensity': float(intStr[0][0])})
    if not len(spotStr) == 0:
        info.update({'spotsize': int(spotStr[0][0])})

    # add file information
    info.update(p)

    # reshape big image into 3D cube of 2D images    
    imgShots = image.shape[0]/imgHeight
    allPx = image.astype(float)
    allPx = allPx.transpose()
    allPx = allPx.reshape((imgHeight,allPx.shape[0],imgShots),order = 'F')
    allPx = np.swapaxes(allPx,0,1)
    
    # get background of averaged stack. this gives good information about
    # background level and inhomogeneity
    statsAvg, ipAvg, all_statsAvg = get_background(allPx.mean(axis=2), rbcSize, roi = {'x': p.x, 'y': p.y, 'w': p.w, 'h': p.h})
    
    avgROI = np.zeros((p.h,p.w)).astype('float32')
    
    for m in range(imgShots):
        
        infom = info.copy()
        
        print('Processing %s (%i out of %i), frame %i') % (p.input_file, k + 1, len(fileList), m + 1)
        
        # grab single shot from image block
        imgPx = allPx[:,:,m]
        
        # correct for burst noise: normally, first frame is not affected
        if cleanBurstNoise and ( (m > 0) or not skipFirst ) :
            corrMask = imgPx > 0
            imgPx[corrMask] = imgPx[corrMask] - bnRef[corrMask]
        
        # crop
        imgROI = imgPx[p.y:(p.y+p.h),p.x:(p.x+p.w)]
            
        # run background extraction for single shot
        statsShot, ipShot, all_statsShot = get_background(imgPx, rbcSize, roi = {'x': p.x, 'y': p.y, 'w': p.w, 'h': p.h})    
        
        if singleCorr:
            bgGrad = ipShot(np.arange(p.x,(p.x+p.w)),np.arange(p.y,(p.y+p.h)))
            bgMean = statsShot['AvgBGMean']
        else:
            bgGrad = ipAvg(np.arange(p.x,(p.x+p.w)),np.arange(p.y,(p.y+p.h)))
            bgMean = statsAvg['AvgBGMean']  
        
        # correct background, phase-flip, normalize        
        if BGCorr and bilinearBGCorr:
            imgROI = imgROI - bgGrad
        elif BGCorr:
            imgROI = imgROI - bgMean

        # Normalize also flips!
        if Normalize:
            imgROI = - imgROI / statsShot['AvgBGStd']
              
        infom.update(statsShot)

        
        fn = p.sub_dir + '_' + p.input_file[:-4] + '_{:03d}'.format(m+1)

        if saveAsTiff:
            infom['micrograph'] = outputFolder + '/' + fn + '.tif'

            imgOut = TiffWriter(path + fn + '.tif')
            imgOut.save(imgROI.astype(np.float32))
            imgOut.close()

        if saveAsMRC:
            infom['micrograph'] = outputFolder + '/' + fn + '.mrc'
            mrc.write(path + fn + '.mrc',imgROI[:,:,np.newaxis])
        
        # append meta data to dict list
        infos.append(infom)
        
        if makeAvgs: avgROI += imgROI
        
    if makeAvgs: avgROI = avgROI / np.sqrt(imgShots)
        
    if saveAsTiff and makeAvgs:
        imgOut = TiffWriter(avgpath + '/' + p.sub_dir + '_' + p.input_file[:-4] + '.tif')
        imgOut.save(avgROI.astype(np.float32))
        imgOut.close()

    if saveAsMRC and makeAvgs:
        mrc.write(avgpath + '/' + p.sub_dir + '_' + p.input_file[:-4] + '.mrc',avgROI[:,:,np.newaxis])    

meta = pd.DataFrame(infos)
meta['ID'] = meta.index
#%%
meta.to_csv(workingFolder + '/meta.csv')
meta.to_pickle(workingFolder + '/meta.pkl')
meta.to_hdf(workingFolder + '/meta.h5','f',mode='w')
copyfile(baseFolder + '/' + sub_dir + '/meta.csv', workingFolder + '/angles.csv')

#starData = pd.DataFrame()
#starData['rlnMicrographName'] = meta['micrograph']
#starData['rlnMicrographId'] = meta['ID']
#starData['rlnMagnification'] = meta['tem_magnification'] * 1.28
#starData['rlnDetectorPixelSize'] = 15.6
#
##starData['rlnDose'] = meta['dose_calibrated']
##starData['sub_dir'] = meta['sub_dir']
##starData['dose_calibrated'] = meta['dose_calibrated']
#
#star.write_star(workingFolder + '/init_meta.star',starData)

#meta.to_hdf(basedir + '/meta.h5','table')